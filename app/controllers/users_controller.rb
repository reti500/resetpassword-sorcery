class UsersController < ApplicationController
	before_action :get_user, only: [:show, :edit, :update, :destroy]

	def index
		@users = User.all
	end

	def new
		@user = User.new
	end

	def create
		@user = User.new( user_params )

		if @user.save
			redirect_to root_url
		else
			render :new
		end
	end

	def show
		
	end

	def edit
		
	end

	def update
		if @user.update( user_params )
      redirect_to root_url, notice: "Cambios guardados"
    else
      render action: 'edit'
    end
	end

	def destroy
		
	end

	private
		def user_params
			params.require( :user ).permit( :username, :email, :password )
		end

		def get_user
			@user = User.find( params[:id] )
		end
end
