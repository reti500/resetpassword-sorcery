class SessionsController < ApplicationController
  def new
  end

  def create
  	user = login( params[:username], params[:password], params[:remember_me] )

  	if user
  		redirect_to root_url
  	else
  		render :new
  	end
  end

  def destroy
  	logout
  	redirect_to root_url
  end

  private
	  def session_params
	  	params.permit( :username, :password, :remember_me )
	  end
end
